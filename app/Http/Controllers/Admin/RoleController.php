<?php 

namespace App\Http\Controllers\Admin;
use Spiderworks\MiniWeb\Controllers\BaseController;
use App\Traits\ResourceTrait;
use View, Redirect, DB;
use App\Models\Roles;
use App\Models\Permissions;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request as HttpRequest;

class RoleController extends BaseController
{
     use ResourceTrait;
     protected $model_path;
    
    public function __construct()
    {
        parent::__construct();
       

        $this->model = new Roles;
        $this->route = 'admin.roles';
        $this->views = 'admin.roles';
        $this->url = "admin/roles/";

        
        $this->resourceConstruct();

    }

    protected function getCollection() {
        return $this->model->select('id', 'name','guard_name', 'created_at', 'updated_at');
        
    }

    protected function setDTData($collection) 
    {
        $route = $this->route;
        return $this->initDTData($collection)
            
            ->rawColumns(['action_edit', 'action_delete']);
    }
    public function create()
    {
        $permissions=Permissions::get();
        return view($this->views . '.form')->with('obj', $this->model)->with('permissions', $permissions);
    }

     public function edit($id) 
    {
        $id = decrypt($id);
        $permissions=Permissions::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
                ->pluck('role_has_permissions.permission_id')
                ->all();
        

        if($obj = $this->model->find($id))
        {

            return view($this->views . '.form')->with('obj', $obj)->with('permissions', $permissions)->with('rolePermissions', $rolePermissions);
        } 
        else 
        {
            return $this->redirect('notfound');
        }
    }
    public function store()
    {
        $this->model->validate();
        $data = request()->all(); 
        $role = Role::create(['name' => $data['name']]);
        $permission = isset($data['permissions'])?$data['permissions']:null;
        $role->syncPermissions($permission);
        return Redirect::to(url('admin/roles/edit', ['id'=>encrypt($role->id)]))->withSuccess('Role successfully added!');
    }

    public function update()
    {
        $data = request()->all(); 
        $this->model->validate(request()->all(), $data['id']);    
        if($role = Role::find(decrypt($data['id'])))
        {
            $role->name = $data['name'];
            $role->save();
            $dataid=$data['id'];       
            $permission = isset($data['permissions'])?$data['permissions']:null;
            $role->syncPermissions($permission);
            return Redirect::to(url('admin/roles/edit', ['id'=>$dataid]))->withSuccess('Role successfully updated!');
             
        }
        else 
        {
                    return Redirect::back()
                    ->withErrors("Ooops..Something wrong happend.Please try again.") // send back all errors to the login form
                    ->withInput(request()->all());
        }
            
    }

    public function checkCodeExist()
    {
        /* RECEIVED VALUE */
         $id = $_REQUEST['id'];
         $code = $_REQUEST['name'];
         
         $where = "name='".$code."'";
         if($id)
            $where .= " AND id != ".$id;
         $resuts = $this->model->whereRaw($where)->get();
         
         if (count($resuts)>0) {  
             echo "false";
         } else {  
             echo "true";
         }
    }

   
    
    }

   


