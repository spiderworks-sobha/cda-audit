<?php
namespace App\Http\Controllers\Admin;

use Spiderworks\MiniWeb\Controllers\BaseController;
use Spiderworks\MiniWeb\Traits\ResourceTrait;
use App\User, Request, View, Redirect, DB, Datatables, Sentinel, Mail, Validator, Image;
use Activation as Act;
use App\Models\Mainpage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request as HttpRequest;


class MainpageController extends BaseController
{
    use ResourceTrait;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Mainpage;

        $this->route = 'admin.mainpage';
        $this->views = 'admin.mainpage';
        $this->url = "admin/mainpage/";

        $this->resourceConstruct();

    }

    protected function getCollection() {
        return $this->model->select('id', 'date', 'title', 'meta_title', 'meta_keyword', 'status');
    }

    protected function setDTData($collection) {
        $route = $this->route;
        return $this->initDTData($collection)
            ->editColumn('status', function($obj) use($route) {
                if($obj->status == 1)
                {
                    return '<a href="' . route($route.'.chang e-status', [encrypt($obj->id)]).'" class="btn btn-success btn-sm miniweb-btn-warning-popup" data-message="Are you sure, want to disable this category?"><i class="fa fa-check-circle"></i></a>';
                }
                else{
                    return '<a href="' . route($route.'.change-status', [encrypt($obj->id)]) . '" class="btn btn-danger btn-sm miniweb-btn-warning-popup" data-message="Are you sure, want to enable this category?"><i class="fa fa-times-circle"></i></a>';
                }
            })
            ->rawColumns(['action_edit', 'action_delete', 'status']);
    }

}
