<?php 
namespace App\Http\Controllers\Admin;

use Spiderworks\MiniWeb\Controllers\BaseController;
use Spiderworks\MiniWeb\Traits\ResourceTrait;
use App\Models\Globalsettings;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request as HttpRequest;
use Redirect;
class GlobalsettingsController extends BaseController
{
    use ResourceTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->model = new Globalsettings;

        $this->route = 'admin.globalsettings';
        $this->views = 'admin.globalsettings';
        $this->url = "admin/globalsettings/";

        $this->resourceConstruct();

    }

    protected function getCollection() {
        return $this->model->select('id', 'email', 'name', 'created_at', 'updated_at', 'banned_at');
        
    }

    protected function setDTData($collection) {
        $route = $this->route;
        return $this->initDTData($collection)
            ->editColumn('updated_at', function($obj) { return date('m/d/Y H:i:s', strtotime($obj->updated_at)); })
            ->editColumn('banned_at', function($obj) use ($route) {
                if($obj->banned_at)
                    return '<a href="' . route( $route . '.change-status',  [encrypt($obj->id)] ) . '" class="btn btn-danger btn-sm miniweb-btn-warning-popup" data-message="Are you sure, want to enable this user?" ><i class="fa fa-times-circle"></i></a>';
                else
                   return '<a href="' . route( $route . '.change-status',  [encrypt($obj->id)] ) . '" class="btn btn-success btn-sm miniweb-btn-warning-popup" data-message="Are you sure, want to disable this user?" ><i class="fa fa-check-circle"></i></a>'; 
            })
            ->rawColumns(['banned_at', 'action_edit', 'action_delete']);
    }

    public function create()
    {
         $obj = Globalsettings::where('id',1)->first();
        return view($this->views . '.form')->with('obj', $this->model)->with('obj', $obj);
    }

  

    public function update(HttpRequest $request)
    {
         $data = $request->all();
        $id = decrypt($data['id']);

            if($obj = $this->model->find($id)){
                $obj->update($data);
               
                 return Redirect::back()->withSuccess('Global Settings successfully updated!');
            } else {
                return Redirect::back()
                        ->withErrors("Ooops..Something wrong happend.Please try again.") // send back all errors to the login form
                        ->withInput(Input::all());
            }
      
    }

}
