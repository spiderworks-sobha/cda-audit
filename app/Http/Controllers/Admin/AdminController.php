<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Input, Request, View, Validator, Redirect, Auth, DB, Session;

class AdminController extends Controller {

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.dashboard');
	}

	public function login()
	{
		if(Auth::user() && Auth::user()->hasRole('Admin'))
		{
			return Redirect::to('admin/dashboard');
		}
		else{
			return view('admin.login');
		}
	}

}
