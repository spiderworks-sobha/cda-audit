<?php

namespace App\Models;

use Spiderworks\MiniWeb\Models\BaseModel;
use Spiderworks\MiniWeb\Traits\ValidationTrait;

class Allpage extends BaseModel
{
    use ValidationTrait {
        ValidationTrait::validate as private parent_validate;
    }

    public function __construct() {

        parent::__construct();
        $this->__validationConstruct();
    }

    protected $table = 'allpages';

    protected $fillable = [
        'banner_image_id', 'date', 'title', 'canonical_name', 'small_description', 'detailed_description', 'meta_title', 'meta_keyword', 'meta_description', 'other_meta_tags', 'status', 'alt', 'primary_image_id','css', 'type',	'parent_id'
    ];

    protected $dates = ['created_at','updated_at'];

    protected function setRules() {

        $this->val_rules = array(
            'meta_keyword' => 'required|max:250',
            'canonical_name' => 'required|max:250',
        );
    }

    protected function setAttributes() {
        $this->val_attributes = array(
        );
    }

    public function validate($data = null, $ignoreId = 'NULL') {
        if( isset($this->val_rules['slug']) )
        {
            $this->val_rules['slug'] = str_replace('ignoreId', $ignoreId, $this->val_rules['slug']);
        }
        return $this->parent_validate($data);
    }

    public function type()
    {
    	return $this->belongsTo('Spiderworks\MiniWeb\Models\Type', 'types_id');
    }

    public function parent()
    {
        return $this->belongsTo('Spiderworks\MiniWeb\Models\Category', 'parent_id');
    }

    public function banner_image()
    {
    	return $this->belongsTo('Spiderworks\MiniWeb\Models\MediaLibrary', 'banner_image_id');
    }

    public function primary_image()
    {
    	return $this->belongsTo('Spiderworks\MiniWeb\Models\MediaLibrary', 'primary_image_id');

    }

    public function menu()
    {
        return $this->morphOne('Spiderworks\MiniWeb\Models\MenuItem', 'linkable');
    }
}
