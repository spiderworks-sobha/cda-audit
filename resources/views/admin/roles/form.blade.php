@extends('spiderworks.miniweb.fileupload')

@section('head')

@endsection

@section('content')
    <div class="container-fluid">

        <div class="col-md-12" style="margin-bottom: 20px;" align="right">
            @if($obj->id)
                <span class="page-heading">EDIT ROLE</span>
            @else
                <span class="page-heading">Create Role</span>
            @endif
            <div >
                <div class="btn-group">
                    <a href="{{route($route.'.index')}}"  class="btn btn-success"><i class="fa fa-list"></i> List
                    </a>
                    @if($obj->id)
                    <a href="{{route($route.'.create')}}" class="btn btn-success"><i class="fa fa-pencil"></i> Create new
                    </a>
                    <a href="{{route($route.'.destroy', [encrypt($obj->id)])}}" class="btn btn-success miniweb-btn-warning-popup" data-message="Are you sure to delete?  Associated data will be removed if it is deleted." data-redirect-url="{{route($route.'.index')}}"><i class="fa fa-trash"></i> Delete</a>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card card-borderless">
                @if($obj->id)
                    <form method="POST" action="{{ route($route.'.update') }}" class="p-t-15" id="RoleFrm" data-validate=true>
                @else
                    <form method="POST" action="{{ route($route.'.store') }}" class="p-t-15" id="RoleFrm" data-validate=true>
                @endif
                @csrf
                <input type="hidden" name="id" @if($obj->id) value="{{encrypt($obj->id)}}" @endif id="inputId">
                
                

                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active show" data-toggle="tab" role="tab"
                           data-target="#tab1Basic"
                        href="#" aria-selected="true">Basic Details</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab1Basic">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Roles</label>
                                        <input type="text" name="name" class="form-control" value="{{$obj->name}}">
                                    </div>
                                </div>
                            </div>
                        
                            

                            <div class="col-md-12">
                                   
                                       <label class="control-label" for="inputLabelEn">Permissions</label>
                                     
                                    <ul class="list-group">

                                        @foreach( $permissions as $permission ) 

                                        <li>
                                            @if(isset($obj->id))
                                               
                                       <input type="checkbox" name="permissions[]" id="{{$permission->id }}" value="{{$permission->id }}" @if(in_array($permission->id, $rolePermissions)) checked="checked" @endif>
                                        @else
                                        <input type="checkbox" name="permissions[]" id="{{$permission->id }}" value="{{$permission->id }}" >
                                        @endif
                                    {{ $permission->name }}

                                          </li>
                                        @endforeach
                                    </ul>
                                    
                                   
                           </div>

                        </div>
                    </div>
                    
                  

                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('bottom')
    <script type="text/javascript">
        var validator = $('#RoleFrm').validate({
              rules: 
              {
                "name": "required",
              },
              messages: 
              {
                "name": "Roles name cannot be blank",
              },
        });
    </script>
@parent
@endsection