@extends('spiderworks.miniweb.fileupload')

@section('head')

@endsection

@section('content')
    <div class="container-fluid">

        <div class="col-md-12" style="margin-bottom: 20px;" align="right">
            @if($obj->id)
                <span class="page-heading">Global Settings</span>
            @endif
        </div>

        <div class="col-lg-12">
            <div class="card card-borderless">
                @if($obj->id)
                    <form method="POST" action="{{ route($route.'.update') }}" class="p-t-15" id="CategoryFrm" data-validate=true>
                @endif
                @csrf
                <input type="hidden" name="id" @if($obj->id) value="{{encrypt($obj->id)}}" @endif id="inputId">

                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active show" data-toggle="tab" role="tab"
                           data-target="#tab1Basic"
                        href="#" aria-selected="true">Basic Details</a>
                    </li>
                    
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab1Basic">
                        <div class="row">
							<div class="col-md-12">
								<div class="row column-seperation padding-5">
									<div class="form-group form-group-default required">
									<label>Header Script</label>
									<textarea name="headerscript" class="form-control richtext" id="top_description" data-image-url="{{route('spiderworks.miniweb.summernote.image')}}">{{$obj->headerscript}}</textarea>
									</div>
								</div>
							</div>                                                                    
							<div class="col-md-12">
								<div class="row column-seperation padding-5">
									<div class="form-group form-group-default required">
									<label>Body Script</label>
									<textarea name="bodyscript" class="form-control richtext" id="bodyscript" data-image-url="{{route('spiderworks.miniweb.summernote.image')}}">{{$obj->bodyscript}}</textarea>
									</div>
								</div>
							</div>                                                                    
							<div class="col-md-12">
								<div class="row column-seperation padding-5">
									<div class="form-group form-group-default required">
									<label>Footer Script</label>
									<textarea name="bodyscript" class="form-control richtext" id="footerscript" data-image-url="{{route('spiderworks.miniweb.summernote.image')}}">{{$obj->footerscript}}</textarea>
									</div>
								</div>
							</div>                       
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
