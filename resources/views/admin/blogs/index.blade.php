@extends('spiderworks.miniweb.app')

@section('content')
    <div class="container-fluid">
        <div class="container">
            <!-- START card -->
            <div class="col-md-12" style="margin-bottom: 20px;" align="right">
                <span class="page-heading">All Blogs</span>
                <div >
                    <div class="btn-group">
                        <a href="{{route($route.'.create')}}" class="btn btn-success"><i class="fa fa-pencil"></i> Create new blog
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card card-borderless padding-15">
                        <table class="table table-hover demo-table-search table-responsive-block" id="datatable"
                               data-datatable-ajax-url="{{ route($route.'.index') }}" >
                            <thead id="column-search">
                            <tr>
                                <th class="table-width-10">ID</th>


                                <th class="table-width-120">date</th>
                                <th class="table-width-120">title</th>
                                <th class="nosort nosearch table-width-10">meta_title</th>
                                <th class="nosort nosearch table-width-10">meta_keyword</th>
                                <th class="nosort nosearch table-width-10">status</th>
                                <th class="nosort nosearch table-width-10">edi</th>
                                <th class="nosort nosearch table-width-10">Delete</th>

                            </tr>

                            <tr>
                                <th class="table-width-10 nosort nosearch"></th>
                                <th class="table-width-10 searchable-input">Slug</th>
                                <th class="table-width-120 searchable-input">Category name</th>
                                <th class="table-width-120 searchable-input">Browser title</th>
                                <th class="table-width-120 searchable-input">Meta Keywords</th>
                                <th class="nosort nosearch table-width-10"></th>
                                <th class="nosort nosearch table-width-10"></th>
                                <th class="nosort nosearch table-width-10"></th>
                            </tr>

                            </thead>

                            <tbody>
                            </tbody>

                        </table>
                </div>
            </div>
            <!-- END card -->
        </div>
    </div>
@endsection
@section('bottom')

    <script>
        var my_columns = [
            {data: null, name: 'id'},
            {data: 'date', name: 'date'},
            {data: 'title', name: 'title'},
            {data: 'meta_title', name: 'meta_title'},
            {data: 'meta_keyword', name: 'meta_keyword'},
            {data: 'status', name: 'status'},
            {data: 'action_edit', name: 'action_edit'},
            {data: 'action_delete', name: 'action_delete'}
        ];
        var slno_i = 0;
        var order = [4, 'desc'];
    </script>
    @parent
@endsection
