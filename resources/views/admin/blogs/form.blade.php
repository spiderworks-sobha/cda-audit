@extends('spiderworks.miniweb.fileupload')

@section('head')

@endsection

@section('content')
    <div class="container-fluid">

        <div class="col-md-12" style="margin-bottom: 20px;" align="right">
            @if($obj->id)
                <span class="page-heading">EDIT Blog</span>
            @else
                <span class="page-heading">Create Blog</span>
            @endif
            <div >
                <div class="btn-group">
                    <a href="{{route($route.'.index')}}"  class="btn btn-success"><i class="fa fa-list"></i> List
                    </a>
                    @if($obj->id)
                    <a href="{{route($route.'.create')}}" class="btn btn-success"><i class="fa fa-pencil"></i> Create new
                    </a>
                    <a href="{{route($route.'.destroy', [encrypt($obj->id)])}}" class="btn btn-success miniweb-btn-warning-popup" data-message="Are you sure to delete?  Associated data will be removed if it is deleted." data-redirect-url="{{route($route.'.index')}}"><i class="fa fa-trash"></i> Delete</a>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card card-borderless">
                @if($obj->id)
                    <form method="POST" action="{{ route($route.'.update') }}" class="p-t-15" id="CategoryFrm" data-validate=true>
                @else
                    <form method="POST" action="{{ route($route.'.store') }}" class="p-t-15" id="CategoryFrm" data-validate=true>
                @endif
                @csrf
                <input type="hidden" name="id" @if($obj->id) value="{{encrypt($obj->id)}}" @endif id="inputId">

                <ul class="nav nav-tabs nav-tabs-simple d-none d-md-flex d-lg-flex d-xl-flex" role="tablist" data-init-reponsive-tabs="dropdownfx">
                    <li class="nav-item">
                        <a class="active show" data-toggle="tab" role="tab"
                           data-target="#tab1Basic"
                        href="#" aria-selected="true">Basic Details</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab2Content"
                        class="" aria-selected="false">Content</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab3SEO"
                        class="" aria-selected="false">SEO</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="tab" role="tab"
                           data-target="#tab4Media"
                           class="" aria-selected="false">Media</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="tab1Basic">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="row column-seperation padding-5">
                                <div class="form-group form-group-default required">
                                  <input type="hidden" name="type" value="0">
                                    <!-- <label>Content type</label>
                                     <select name="type" class="full-width miniweb-select2-input"  data-placeholder="Select Parent Category">
                                             <option @if($obj->type=="page") value="page" selected="selected" @else value="page"  @endif >Page</option>
                                            <option @if($obj->type=="blog") value="blog" selected="selected" @else value="blog"  @endif >Blog</option>
                                    </select> -->
                                    <label>Parent Category</label>
                                    <select type="text" name="parent_id"  class="full-width miniweb-select2-input" id="parent_id" name="parent_id">
                                    @if(isset($parent))
                                    <option value="0">select</option>
@foreach($parent as $c)
<option value="{{ $c->id }}" @if(isset($obj))   @if($c->id==$obj->parent_id) selected='selected' @endif @endif>{{ $c->title }}</option>
@endforeach
@endif
                                </select>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>DATE</label>
                                        <input type="date" name="date" class="form-control" value="{{$obj->date}}" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label class="">canonical_name</label>
                                        <input type="text" name="canonical_name" class="form-control" value="{{$obj->canonical_name}}" id="slug">
                                    </div>
                                    <p class="hint-text small">The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>
                                </div>
                            </div>
                            @if(config('miniweb.category_types'))
                                <div class="col-md-6">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default form-group-default-select2">
                                            <label class="">Category Type</label>
                                            <select name="types_id" class="full-width miniweb-select2-input" data-select2-url="{{route('spiderworks.miniweb.select2.types')}}" data-placeholder="Select Category Type">
                                                @if($obj->type)
                                                    <option value="{{$obj->types_id}}" selected="selected">{{$obj->type->name}}</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Title</label>
                                        <input type="text" name="title" class="form-control" value="{{$obj->title}}" id="title">
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Image Alt</label>
                                        <textarea name="description" class="form-control" rows="3" id="description">{{$obj->description}}</textarea>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label class="">Parent Category</label>
                                        <select name="parent_id" class="full-width miniweb-select2-input" data-select2-url="{{route('spiderworks.miniweb.select2.categories')}}" data-placeholder="Select Parent Category">
                                            @if($obj->parent)
                                                <option value="{{$obj->parent_id}}" selected="selected">{{$obj->parent->name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2Content">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default required">
                                            <label>Small Description</label>
                                            <textarea name="small_description" class="form-control richtext" id="small_description" data-image-url="{{route('spiderworks.miniweb.summernote.image')}}">{{$obj->small_description}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default required">
                                            <label>Detailed Description</label>
                                            <textarea name="detailed_description" class="form-control richtext" id="detailed_description" data-image-url="{{route('spiderworks.miniweb.summernote.image')}}">{{$obj->detailed_description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default">
                                            <label class="">Extra Css</label>
                                        <textarea type="text" name="css" class="form-control" rows="3" id="css">
                                          {!!$obj->css!!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row column-seperation padding-5">
                                        <div class="form-group form-group-default">
                                            <label class="">Extra Js</label>
                                            <textarea name="extra_js" class="form-control" rows="3" id="extra_js">{{$obj->extra_js}}</textarea>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3SEO">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label>Meta title</label>
                                        <input type="text" class="form-control" name="meta_title" id="meta_title" value="{{$obj->meta_title}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta Keywords</label>
                                        <textarea name="meta_keyword" class="form-control" rows="3" id="meta_keyword">{{$obj->meta_keyword}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default">
                                        <label class="">Meta description</label>
                                        <textarea name="meta_description" class="form-control" rows="3" id="meta_description">{{$obj->meta_description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row column-seperation padding-5">
                                    <div class="form-group form-group-default required">
                                        <label>Other Meta Tags</label>
                                        <textarea name="other_meta_tags" class="form-control richtext" id="other_meta_tags" data-image-url="{{route('spiderworks.miniweb.summernote.image')}}">{{$obj->other_meta_tags}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab4Media">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <p class="col-md-12">Banner Image</p>
                                    <div class="default-image-holder padding-5">
                                        <a href="javascript:void(0);" class="image-remove"><i class="fa  fa-times-circle"></i></a>
                                        <a href="{{route('spiderworks.miniweb.media.popup', ['popup_type'=>'single_image', 'type'=>'Image', 'holder_attr'=>'0', 'related_id'=>$obj->id])}}" class="miniweb-open-ajax-popup" title="Media Images" data-popup-size="large" id="image-holder-0">
                                          @if($obj->banner_image_id && $obj->banner_image)
                                            <img class="card-img-top padding-20" src="{{ asset('public/'.$obj->banner_image->thumb_file_path) }}">
                                          @else
                                            <img class="card-img-top padding-20" src="{{asset('miniweb/img/add_image.png')}}">
                                          @endif
                                        </a>
                                        <input type="hidden" name="banner_image_id" id="mediaId0" value="{{$obj->banner_image_id}}">
                                    </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <p class="col-md-12">Primary Image</p>
                                    <div class="default-image-holder padding-5">
                                        <a href="javascript:void(0);" class="image-remove"><i class="fa  fa-times-circle"></i></a>
                                        <a href="{{route('spiderworks.miniweb.media.popup', ['popup_type'=>'single_image', 'type'=>'Image', 'holder_attr'=>'1', 'related_id'=>$obj->id])}}" class="miniweb-open-ajax-popup" title="Media Images" data-popup-size="large" id="image-holder-1">
                                          @if($obj->primary_image_id && $obj->primary_image)
                                            <img class="card-img-top padding-20" src="{{ asset('public/'.$obj->primary_image->thumb_file_path) }}">
                                          @else
                                            <img class="card-img-top padding-20" src="{{asset('miniweb/img/add_image.png')}}">
                                          @endif
                                        </a>
                                        <input type="hidden" name="primary_image_id" id="mediaId1" value="{{$obj->primary_image_id}}">
                                    </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('bottom')
    <script type="text/javascript">
        var validator = $('#CategoryFrm').validate({
              rules: {
                "canonical_name": "required",
                slug: {
                  required: true,
                  remote: {
                      url: "{{route('spiderworks.miniweb.unique.category-slug')}}",
                      data: {
                        id: function() {
                          return $( "#inputId" ).val();
                      }
                    }
                  }
                },
              },
              messages: {
                "name": "Category name cannot be blank",
                slug: {
                  required: "Slug cannot be blank",
                  remote: "Slug is already in use",
                },
              },
            });
    </script>
@parent
@endsection
