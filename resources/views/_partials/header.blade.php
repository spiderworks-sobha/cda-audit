

<header class="header">

<section class="top-section">
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-6 col-9">
<div class="top-cont-left">“Delivering solutions where it counts “</div>
</div>
<div class="col-md-8 col-sm-6 col-3 mobfull">
<div class="top-right-section">
<div class="free-consultation">
<a href="{{ asset('assets/uploads/brochure/cdaaudit.pdf') }}" download><span>Download Brochure</span></a>
<a href="consultation.html"><span>Free One Hour Consultation</span></a> </div>
<div class="top-contact"><a href="cdn-cgi/l/email-protection.html#d6bfb8b0b996b5b2b7b7a3b2bfa2f8b5b9bb" class="link"><span class="span"><span class="__cf_email__" data-cfemail="1e777078715e7d7a7f7f6b7a776a307d7173">[email&#160;protected]</span></span></a></div>
<div class="top-contact"><a href="tel:+971 426 100 89" class="link link2"><span class="span">+971 426 100 89</span></a></div>
</div>
</div>
</div>
</div>
</section>

<div class="container">
<div class="head-middle-section">
<div class="row">
<div class="col-lg-3 col-md-2">
<div class="logo">

<a href="index.html"><img src="{{asset('assets/images/logo.png')}} "alt="CDA" title="CDA" class="img-fluid"></a> </div>


<button class="navbar-toggler navbar-toggler-right menu-button" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
<div class="main-icon-bar"> <i class="fa fa-bars"></i></div>
</button>
</div>
<div class="col-lg-9 col-md-10">
<div class="nav-section">
<div class="main-nav-section">
<nav class="navbar navbar-toggleable-lg navbar-light bg-faded navbar-expand-md">
<div class="collapse navbar-collapse" id="navbarNavDropdown">
<ul class="navbar-nav">
<li>
<a class="" href="index.html">Home</a> </li>
<li>
<a class="active" href="about-us.html">About Us</a> </li>
<li class="dropdown"> <a href="#" class="" data-toggle="dropdown">Services</a>
<ul class="dropdown-menu animated2 fadeInUp">
<li>
<a class="dropdown-item" href="service/advisory-services-or-cfo-services.html">Advisory Services or CFO services</a> </li>
<li>
<a class="dropdown-item" href="service/management-accounting-bookkeeping.html">Management Accounting & Bookkeeping</a> </li>
<li>
<a class="dropdown-item" href="service/internal-audit-restructuring-corporate.html">Internal Audit & Restructuring Corporate</a> </li>
<li>
<a class="dropdown-item" href="service/vat-consultancy-tax-audit.html">VAT Consultancy & Tax Audit</a> </li>
<li>
<a class="dropdown-item" href="service/accounting-software-support.html">Accounting Software Support</a> </li>
<li>
<a class="dropdown-item" href="service/due-diligence-valuation-services.html">Due Diligence & Valuation Services</a> </li>
</ul>
</li>
<li>
<a class="" href="why-CDA.html">Why cda</a> </li>
<li>
<li>
<a class="" href="our-team.html">Our team</a> </li>
<li>
<a class="" href="blog.html">Blog</a> </li>
<li>
<a class="" href="career.html">Careers</a> </li>
<li>
<a class="" href="contact-us.html">Contact Us</a> </li>
</ul>
</div>
</nav>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="main_head navbar-custom fixed-top" role="navigation">
<div class="header-fixed animated2 fadeInUp">
<div class="container">
<div class="head-middle-section">
<div class="row">
<div class="col-md-3">
<div class="logo">
<a href="index.html"><img src="{{asset('assets/images/logo.png')}}" alt="CDA" title="CDA" class="img-fluid"></a> </div>
<button class="navbar-toggler navbar-toggler-right menu-button" type="button" data-toggle="collapse" data-target="#navbarNavDropdown2" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
<div class="main-icon-bar"> <i class="fa fa-bars"></i></div>
</button>
</div>
<div class="col-md-9">
<div class="nav-section">
<div class="main-nav-section">
<nav class="navbar navbar-toggleable-lg navbar-light bg-faded navbar-expand-md">
<div class="collapse navbar-collapse" id="navbarNavDropdown2">
<ul class="navbar-nav">
<li>
<a class="" href="index.html">Home</a> </li>
<li>
<a class="active" href="about-us.html">About Us</a> </li>
<li class="dropdown"> <a href="#" class="" data-toggle="dropdown">Services</a>
<ul class="dropdown-menu animated2 fadeInUp">
<li>
<a class="dropdown-item" href="service/advisory-services-or-cfo-services.html">Advisory Services or CFO services</a> </li>
<li>
<a class="dropdown-item" href="service/management-accounting-bookkeeping.html">Management Accounting & Bookkeeping</a> </li>
<li>
<a class="dropdown-item" href="service/internal-audit-restructuring-corporate.html">Internal Audit & Restructuring Corporate</a> </li>
<li>
<a class="dropdown-item" href="service/vat-consultancy-tax-audit.html">VAT Consultancy & Tax Audit</a> </li>
<li>
<a class="dropdown-item" href="service/accounting-software-support.html">Accounting Software Support</a> </li>
<li>
<a class="dropdown-item" href="service/due-diligence-valuation-services.html">Due Diligence & Valuation Services</a> </li>
</ul>
</li>
<li>
<a class="" href="why-CDA.html">Why cda</a> </li>
<li>
<a class="" href="our-team.html">Our team</a> </li>
<li>
<a class="" href="blog.html">Blog</a> </li>
<li>
<a class="" href="career.html">Careers</a> </li>
<li>
<a class="" href="contact-us.html">Contact Us</a> </li>
</ul>
</div>
</nav>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


</header>
