<footer id="footer" class="footer" data-parallax="scroll" data-image-src="{{asset('assets/images/bg/footer.jpg')}}">
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-12">
<div class="foot-about">
<div class="foot-logo">

<a href="index.html"><img src="{{asset('assets/images/foot-logo.png')}}" alt="footer logo" class="img-fluid" /></a> </div>
<p>CDA Accounting and Bookkeeping Services LLC is a central hub for all the Management Consultant Services you need in Dubai. From Advisory Services to Tax Auditing, we offer a wide variety of services to an inclusive clientele.
</p>
</div>
</div>
<div class="col-md-2 col-sm-6">
<div class="quick-links">
<h6 class="title">QUICK LINK</h6>
<ul>
<li><a href="about-us.html">About US</a></li>
<li><a href="why-CDA.html">Why CDA</a></li>
<li><a href="contact-us.html">Contact US</a></li>
<li><a href="privacy-policy.html">Privacy Policy</a></li>
<li><a href="terms-and-conditions.html">Terms & Conditions</a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="quick-links">
<h6 class="title">our Services</h6>
<ul>
<li>
<a class="" href="our-service/accounting-services-in-dubai.html">Accounting Services in Dubai</a> </li>
<li>
<a class="" href="our-service/vat-registration-in-dubai.html">VAT Registration in Dubai</a> </li>
<li>
<a class="" href="our-service/bookkeeping-services-dubai.html">Bookkeeping Services Dubai</a> </li>
<li>
<a class="" href="our-service/accounting-outsourcing-in-dubai.html">Accounting Outsourcing in Dubai</a> </li>
<li>
<a class="" href="our-service/tax-consultant-dubai.html">Tax Consultant Dubai</a> </li>
 <li>
<a class="" href="our-service/auditing-companies-in-dubai.html">Auditing Companies in Dubai</a> </li>
<li>
<a class="" href="our-service/internal-audit-dubai.html">Internal Audit Dubai</a> </li>
<li>
<a class="" href="our-service/vat-consultants-in-dubai.html">VAT Consultants in Dubai</a> </li>
</ul>
</div>
</div>
<div class="col-md-3">
<div class="foot-address">
<h6 class="title">CONTACT US</h6>
<div class="address">40th floor, Citadel tower Business Bay Dubai, PO Box : 5586</div>
<div class="phone">Phone No: +971 426 100 89</div>
<div class="phone">Mobile No: +971 557 188 763‎</div>
<div class="mail">E-mail: <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="aec7c0c8c1eecdcacfcfdbcac7da80cdc1c3">[email&#160;protected]</a></div>
<a href="{{ asset('assets/uploads/brochure/cdaaudit.pdf') }}" class="dwld-brchr" download>Download brochure</a>
</div>
</div>
</div>
<div class="row">
<div class="col-md-4">
<div class="follow-btm">
<h6 class="title2">Follow us</h6>
<ul>
<li> <a href="https://www.facebook.com/cdaaudit/" target="_blank"> <i class="fab fa-facebook-f"></i> </a></li>
<li> <a href="https://twitter.com/cda_llc" target="_blank"> <i class="fab fa-twitter"></i> </a></li>
<li> <a href="https://www.linkedin.com/in/cda-accounting-and-bookkeeping-services-a51391172/" target="_blank"> <i class="fab fa-linkedin"></i> </a></li>
<li> <a href="https://www.instagram.com/cda_accounting_bookkeeping/" target="_blank"> <i class="fab fa-instagram"></i> </a></li>
</ul>
</div>
</div>
<div class="col-md-8">
<form class="news-letter" method="post">
<h6 class="title2">Newsletter</h6>
<div class="input-group mb-3">
<input type="email" id="news-email" name="email" class="form-control" placeholder="Your email address" aria-label="Your email address" aria-describedby="basic-addon2" required>
<div class="input-group-append">
<button class="btn btn-outline-secondary" type="submit"></button>
</div>
</div>
</form>
</div>
</div>
</div>
<style>
.cdafoot {
margin: 40px auto 0;
max-width: 720px;
}
.cda {
  overflow: auto;
  list-style-type: none;
}
.cda li {
line-height: 20px;
    float: left;
    margin-right: 0px;
    border-right: 1px solid #aaa;
    padding: 0 20px;
    margin: 5px 0px;
}
.cda li:last-child {
  border-right: none;
}
.cda li a {
  text-decoration: none;
  color: #cdcdcd;
  -webkit-transition: all 0.5s ease;
}
.cda li a:hover {
  color: #fff;
}
.cda li.active a {
  font-weight: bold;
  color: #333;
}
.site-footer .container{
position:relative;
}
</style>
<div class="container">
<div class="row">
<nav class="cdafoot">
<ul class="cda">
<li><a href="business-services.html">Business Services</a></li>
<li><a href="accounting.html">Accounting</a></li>
<li><a href="auditing.html">Auditing</a></li>
<li><a href="vat.html">VAT</a></li>
<li><a href="accounting-software.html">Accounting Software</a></li>
<li><a href="due-diligence.html">Due Diligence</a></li>
</ul>
</nav>
</div>
</div>
<div class="copyright">
<div class="container">
<p><span id="copyright"> <script data-cfasync="false" src="cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script> </span> Copyright © <span>CDA Audit</span>. All Rights Reserved.</p>
</div>
</div>
</footer>
