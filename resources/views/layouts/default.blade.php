
<!DOCTYPE html>
<html lang="en-US">
<head>
<link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  @yield('meta-data')


<title>Best Financial Services Consulting Firm in Dubai | Accounting Services in Dubai : CDA</title>
 <script src="{{ asset('public/assets/js/jquery.min.js')}}"></script>

<script type="text/javascript">
            var homeUrl = 'index.html';
        </script>
<meta name="keywords" content="Financial Services Consulting Firm in Dubai, Advisory Services in Dubai, internal audit abu dhabi, vat consulltant abu dhabi, accounting  outsourcing in abu dhabi">
<meta name="description" content="CDA Accounting &amp; Bookkeeping LLC is one of the top providers of accounting services in Dubai, accounting services in Abu Dhabi, VAT consultation in Dubai, Internal Audit Dubai, etc. Our success is measured by the value of services that we deliver to our clients.">
<meta name="csrf-param" content="_csrf-frontend">
<meta name="csrf-token" content="f9bJ560en2PNWjnlyxSawCERjpQmIMJbcmwwuUflmv9G5ISpwErJJ6YWTtaqLcm5d2j2-WtU7x0EA1v7MY_zlg==">
<link rel="stylesheet" href="{{ asset('assets/bootstrap/4.2.1/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/bootstrap/4.2.1/css/all.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/bootstrap/4.2.1/css/animate.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/bootstrap/4.2.1/css/slick.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/stylesheet.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/bootstrap/4.2.1/css/responsive.css') }}" />
<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet"> <link rel="canonical" href="index.html">
 <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TDLDDQ9');</script>

<script type="application/ld+json">
            {
            "@context": "http://schema.org/",
            "@type": "WebSite",
            "name": "CDA Accountinf & Bookkeeping Services LLC",
            "alternateName": "Charles & Darwish Associates",
            "url": "https://www.cdaaudit.com/",
            "potentialAction": {
            "@type": "SearchAction",
            "target": "https://www.cdaaudit.com/why-CDA{search_term_string}",
            "query-input": "required name=search_term_string"
            }
            }
        </script>
<meta property="og:type" content="business.business">
<meta property="og:title" content="CDA Accounting and Bookkeeping Services LLC">
<meta property="og:url" content="https://www.cdaaudit.com/">
<meta property="og:image" content="https://www.cdaaudit.com/images/logo.png">
<meta property="business:contact_data:street_address" content="40th Floor, Citadel Tower Business Bay Dubai, PO Box : 5586">
<meta property="business:contact_data:locality" content="Business Bay">
<meta property="business:contact_data:region" content="Dubai">
<meta property="business:contact_data:postal_code" content="5586">
<meta property="business:contact_data:country_name" content="United Arab Emirates">
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="CDA Accounting and Bookkeeping Services LLC">
<meta name="twitter:title" content="CDA Accounting and Bookkeeping Services LLC">
<meta name="twitter:description" content="CDA Accounting and Bookkeeping Services LLC is a one-stop solution provider for all accounting services in Dubai, accounting services in Abu Dhabi, TAX consultation in Dubai.">
<meta name="twitter:image" content="https://www.cdaaudit.com/images/logo.png">
<meta name="twitter:image:alt" content="CDA Accounting and Bookkeeping Services LLC"> <script type='application/ld+json'>
            {
            "@context": "http://www.schema.org",
            "@type": "AccountingService",
            "name": "CDA Accounting & Bookkeeping LLC",
            "url": "https://www.cdaaudit.com/",
            "sameAs": [
            "https://www.cdaaudit.com/why-CDA"
            ],
            "logo": "https://www.cdaaudit.com/images/logo.png",
            "description": "CDA aims to be leader in providing accounting &amp; bookkeeping services in Dubai, UAE. We also provide management consulting services,VAT, auditing with years of experience.",
            "address": {
            "@type": "PostalAddress",
            "streetAddress": "40th floor, Citadel tower, Business Bay, Dubai, PO Box : 5586",
            "addressLocality": "Business Bay",
            "addressRegion": "Dubai",
            "postalCode": "5586",
            "addressCountry": "UAE"
            },
            "openingHours": "Mo, Tu, We, Th, Sa, Su -",
            "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+971 426 100 89",
            "contactType": "info@cdaaudit.com"
            }
            }
        </script>
</head>
<style media="screen">
.pages h3 {
font-size: 18px;
font-weight: bold;
}
.pages a.more-btn.collapsed .more {
display: inline;
}
.pages a.more-btn.collapsed .less {
display: none;
}
.pages a.more-btn .more {
display: none;
}
.pages a.more-btn .less {
display: inline;
}
</style>

<body>

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDLDDQ9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<div class="main-contact-icon-right-sd">
<div class="call-right quick-contact">
<a class="slide-left" href="tel:+971 557 188 763‎"><span>+971 557 188 763‎</span></a>
</div>
<div class="mail-right quick-contact">
<a class="slide-left" href="cdn-cgi/l/email-protection.html#d1b8bfb7be91b2b5b0b0a4b5b8a5ffb2bebc"><span><span class="__cf_email__" data-cfemail="1871767e77587b7c79796d7c716c367b7775">[email&#160;protected]</span></span></a>
</div>
</div>

<body>
   	@include('_partials.header')

    @yield('content')

    @include('_partials.footer')

    <a href="#" class="scrollup">Scroll</a>

    <script type="text/javascript">
        $(document).ready(function () {

            $(window).scroll(function () {

                if ($(this).scrollTop() > 100) {

                    $('.scrollup').fadeIn();
                } else {
                    $('.scrollup').fadeOut();
                }
            });

            $('.scrollup').click(function () {
                $("html, body").animate({
                    scrollTop: 0
                }, 1000);
                return false;
            });

        });
    </script>

    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5c6a376f77e0730ce04368dd/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>

    <script src="http://192.168.1.208/cda_audit/assets/288fef16/jquery.js"></script>


    <script src="http://192.168.1.208/cda_audit/assets/68fc822e/yii.js"></script>

    <script src="http://192.168.1.208/cda_audit/assets/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>


    <script src="http://192.168.1.208/cda_audit/assets/bootstrap/4.2.1/js/bootstrap.min.js"></script>

    <script src="http://192.168.1.208/cda_audit/assets/ajax/libs/slick-carousel/1.9.0/slick.js"></script>



    <script src="http://192.168.1.208/cda_audit/assets/footer/js/grayscale.js"></script>


    <script src="http://192.168.1.208/cda_audit/assets/footer/js/parallax.js"></script>

    <script src="www.google.com/recaptcha/api.js"></script>
        <script src="{{asset('assets/footer/js/script.js')}}"></script> </body>

        <script>
            $(document).ready(function () {
                $(document).on('submit', '.contact-enquiry', function (e) {
                    e.preventDefault();
                    var res = grecaptcha.getResponse();
                    if (res == "" || res == undefined || res.length == 0)
                    {
                        if ($("#recaptcha").next(".validation").length == 0) // only add if not added
                        {
                            $("#recaptcha").after("<div class='validation' style='color:#c54040;bottom: 50px;font-weight: 600;position: absolute;font-size: 13px;margin-bottom: 14px;'>Please verify that you are not a robot</div>");
                        }
                    } else {
                        var str = $(this).serialize();
                        $.ajax({
                            type: "POST",
                            url: '/site/contact-enquiry',
                            data: str,
                            success: function (data)
                            {
                                if (data == 1) {
                                    $('.contact-enquiry').before('<div id="email-alert" style="">Your Contact Enquiry Send Successfully</div>');
                                }
                                $('#name').val("");
                                $('#email').val("");
                                $('#phone').val("");
                                $('#company').val("");
                                $('#message').val("");
                                $('#email-alert').delay(1000).fadeOut('slow');
                                $(".validation").remove();
                            }
                        });
                    }
                });

                $(document).on('submit', '.news-letter', function (e) {
                    e.preventDefault();
                    var str = $(this).serialize();
                    $.ajax({
                        type: "POST",
                        url: '/site/news-letter',
                        data: str,
                        success: function (data)
                        {
                            if (data == 1) {
                                $('.news-letter').append('<div id="email-alert" style="">Your Request Send Successfully</div>');
                            } else if (data == 2) {
                                $('.news-letter').append('<div id="email-alert" style="">Already Send Rrequest</div>');
                            }
                            $('#news-email').val("");
                            $('#email-alert').delay(1000).fadeOut('slow');
                        }
                    });
                });
            });
        </script>

</body>

<!-- Mirrored from www.cdaaudit.com/about-us by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Jan 2020 10:29:18 GMT -->

</html>
