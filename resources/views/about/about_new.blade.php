
@extends('layouts.default')
@section('meta-data')
  <title></title>
  <meta name="description" content="">
  <meta name="keywords" content="">
@endsection
@section('content')
<style>
    .banner{
      background: url(' {{ asset('assets/uploads/baner_images/about.jpg') }} ');
    }
</style>
<div id="about" class="inner">
<section class="banner">
<div class="container">
<div class="row">
<div class="banner-content">
<h1 class="page-title">About Us</h1>
<nav aria-label="breadcrumb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Home</a></li>
<li class="breadcrumb-item active" aria-current="About us">About us</li>
</ol>
</nav>
</div>
</div>
</div>
</section>

<section class="welcome-sec">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="welcome-content welcome-content-about">
<div class="head-style1 center-cntnt">
<h6>Our story</h6>
<h2 class="main-title">Welcome to <span>CDA</span> Accounting and <br>Bookkeeping Services LLC </h2>
</div>
<div class="row">
<div class="col-md-12">
<p style="text-align:justify"><strong>CDA Accounting and Bookkeeping Services LLC</strong> is a central hub for all the Management Consulting and Accounting Services you need in Dubai. From Accounting to Bookkeeping, Advisory Services to Internal Auditing, Tax Consultation to help in VAT Registration, we offer highly professional &amp;&nbsp;valued services to an inclusive clientele. Powered by a group of professionals who are not just skilled but also passionate about the services they excel at, CDA can offer the best experience and the results. No matter whether you are looking for Accounting Services in Dubai or Accounting Services in Abu Dhabi, Tax Consultant Dubai or Tax Consultant Abu Dhabi, CFO Services, Accounting&nbsp;Outsourcing in Dubai or Accounting Outsourcing in Abu Dhabi, or simply the Standard Bookkeeping Services for your business, CDA is capable of delivering.</p>
<p style="text-align:justify">This company was founded and mentored by Mr. Mohammad Darwish, who worked as the Finance Director for the Office of Shaik Zayed, Abu Dhabi. Since the beginning, it has been offering quality services to a different portfolio of companies in UAE and we have associated companies registered in the UK as well as in India. A registered professional service organization in Dubai, CDA has been designed to deliver sustainable financial discipline and control to its customers when it comes to accounting. CDA is also an expert when it comes to the various budgeting, cost-benefit analysis, VAT registration&nbsp;projects you may have.</p>
<p>A group of people who are qualified and experienced in areas such as Auditing and Assurance, Accounting and Advisory, and Business and Management Consultancy is the real strength behind CDA. The experience in International Financial&nbsp;Reporting Standards and UAE VAT laws helps the team deliver the best results regardless of where your company is based out of.</p>
<p>There is a reason why CDA Accounting and Bookkeeping Services LLC pops up when you search for the best accounting &amp;&nbsp;bookkeeping services near me. Through the years of experience, CDA has understood the importance of Confidentiality and Integrity when it comes to different accounting services in Dubai and accounting services in Abu Dhabi. CDA also boasts one of the top customer retention rates among accounting and bookkeeping firms in Dubai.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="speciality parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/images/bg/speciality.jpg')}}">
<div class="container">
<div class="row">
<div class="col-md-4">
<div class="special-box">

<icon><img src="{{asset('assets/images/icon/special-icon1.png')}}" alt="Who we are" class="img-fluid" /></icon>
<div class="content">
<h5 class="title">Who We Are</h5>
<p>We are a group of professionals who try to offer the best financial consultancy services to our clientele. We offer standard and custom-tailored accounting services, based on the specific requirements of our clients. </p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="special-box">
<icon><img src="{{asset('assets/images/icon/special-icon2.png')}}" alt="Who we are" class="img-fluid" /></icon>
<div class="content">
<h5 class="title">What We Are</h5>
<p>CDA Accounting and Bookkeeping Services LLC is one of the trusted and popular Accounting firms in Dubai & serving across the UAE with a variety of clients. </p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="special-box">
<icon><img src="{{asset('assets/images/icon/special-icon2.png')}}" alt="Who we are" class="img-fluid" /></icon>
<div class="content">
<h5 class="title">Our approach</h5>
<p>We follow a beyond-the-numbers approach that analyzes the beneficiary organization first and then delivers the custom-tailored actions, ensuring the best results.</p>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="why-choose">
<div class="container">
<div class="row">
<div class="col-12">
<h2 class="main-title">Why choose us <span>CDA</span></h2>
</div>
</div>
<div class="row">
<div class="col-lg-7">
<div class="y-choose-cntnt">
<p>For the past five years, CDA Accounting and Bookkeeping Services LLC has been one of the reputed&nbsp;providers of management consulting &amp; accounting services in Dubai. It has been possible due to some values that we follow at CDA, starting from expertise-filled professionals until the streamlined execution of the services. Be it CFO Services, VAT registration or some internal auditing for your business, our team has years of experience to help you.</p>
<ul>
<li>An Experienced Team</li>
<li>Wide Exposure to Market</li>
<li>Custom-Tailored Services</li>
<li>Quick &amp; Trusted Support</li>
<li>A Diverse Clientele</li>
<li>One-Stop Solution Provider</li>
</ul>
</div>
</div>
<div class="col-lg-5">
<div class="testimonial">
<div class="carousel-controls testimonial-carousel-controls">
<div class="testimonial-carousel">
<div class="one-slide mx-auto">
<div class="d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
<div class="story-box">
<div class="testi-icon"></div>
<div class="testi-content">
<p>CDA is a highly professional company for Accounting and Bookkeeping services across UAE. I personally experienced their result-oriented approach and the services from CDA have been just awesome. On any day, I’d recommend their services.CDA is the best accounting firm in dubai. </p>
</div>
<div class="testi-footer">
<h4 class="title">Jithin Jacob</h4>
<h4 class="sub-title">Assistant Manager - KPMG</h4>
</div>
</div>
</div>
</div>
<div class="one-slide mx-auto">
<div class="d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
<div class="story-box">
<div class="testi-icon"></div>
<div class="testi-content">
<p>When it comes to financial and accounting matters nobody can compete with him. I am sure that Charles & his team can satisfy all the clients as well. In my opinion he is very good in controlling cost & advising for better decision making. Such a hardworking and dedicated individual is just a reason why we love the whole experience from CDA, the best accounting company in Dubai offers outstanding accounting services in UAE.</p>
</div>
<div class="testi-footer">
<h4 class="title">Nabil Yasin</h4>
<h4 class="sub-title">Managing Partner - Al Nahdah National Insurance Brokers Co. LLC</h4>
</div>
</div>
</div>
</div>
<div class="one-slide mx-auto">
<div class="d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
<div class="story-box">
<div class="testi-icon"></div>
<div class="testi-content">
<p>CDA is really great at completing works before deadline. At the same time, CDA gave us exceptional accuracy, highly responsive, highly professional and experts in IFRS & UAE VAT Laws. It’s also great to have reports and schedules that meet our requirements. Altogether, accounting services in Dubai from CDA is worth having at any cost.</p>
</div>
<div class="testi-footer">
<h4 class="title">Thomas Esow</h4>
<h4 class="sub-title">Finace Director - Al Andalous Electromechanical Works Co. LLC</h4>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="our-mission">
<div class="container">
<div class="row">
<div class="col-lg-12 social-responsibility">
<div class="head-style1 center-cntnt">
<h6>Our story</h6>
<h2 class="main-title">Corporate social responsibility </h2>
</div>
<div class="content">
<p style="text-align:justify">At <strong>CDA Accounting and Bookkeeping Services LLC</strong>, we do not take nature, the society or our employees for granted. We follow a number of steps to make sure that CDA protects the environment, provides back to the community and preserves humane values that matter. Our Corporate Social Responsibility sector has been made up of a few sub-areas, each of which is&nbsp;dedicated to the Community, Team, and Environment.</p>
<br>
<p style="text-align:justify">Giving back to the community, CDA has launched a number of charity programs. CDA also volunteers a few networking events that work as a platform for bringing together bright minds. In addition to this, CDA Accounting and Bookkeeping Services LLC&nbsp;offers a healthy work environment. Paperless billing and power-efficient office equipment make it easy for CDA to enhance the overall nature-friendliness.</p>
</div>
</div>
<div class="col-lg-12">
<div class="vision">
<div class="head-style1">
<h2 class="main-title">Vision & Mission</h2>
</div>
<div class="content">
<p style="text-align:justify"><strong>CDA Accounting and Bookkeeping Services LLC</strong> was founded with a vision of providing quality Bookkeeping,&nbsp;Financial Consultancy, Accounting services to both regional firms in UAE and international organizations. It also envisions a world that has financial stability, discipline, and transparency.</p>
<p style="text-align:justify">&nbsp;</p>
<p style="text-align:justify">The mission of CDA Accounting and Bookkeeping Services LLC is to become a comprehensive and centralized hub of accounting, bookkeeping, outsourcing, tax consultation, and other finance-related services. We aim to create and break records as it comes to ethical values, performance and the ultimate quality of the services offered. CDA also wants to build a platform that combines passionate and skilled professionals, who have expertise in offering financial services to a diverse clientele.</p>
<p style="text-align:justify">&nbsp;</p>
<p style="text-align:justify">We also follow a bunch of core values at CDA Accounting and Bookkeeping Services LLC.</p>
<p style="text-align:justify"><strong>Quality, Delivered</strong> &mdash; CDA wants to provide the highest-quality service to its customers, no matter the nature or volume of the project. Our teams have sufficient skill-set and are trained to work towards the ultimate satisfaction of our clients.</p>
<p style="text-align:justify">&nbsp;</p>
<p style="text-align:justify"><strong>Adapt, Learn</strong> &mdash; With all due respect to the conventions, CDA believes change is inevitable. One of our core values is to learn with the world and adapt according to the changes. From technical infrastructure to various strategic approaches, CDA has taken turns that matter.</p>
<p style="text-align:justify">&nbsp;</p>
<p style="text-align:justify"><strong>Respect &amp; Credibility</strong> &mdash; We deliver respect to every client that approaches CDA. Our idea is to give priority to everyone, as the world is an equal place. This helps CDA become a credible organization in the long run.</p>
<p style="text-align:justify">&nbsp;</p>
<p style="text-align:justify"><strong>Unique</strong> &mdash; CDA Accounting and Bookkeeping Services LLC aims to become a different accounting company in UAE, for all the right reasons. From adapting to the environmental changes to offering services that meet with current demands, everything is made with a difference.</p>
</div>
</div>
</div>
</div>
</div>
</section>

</div>
@endsection
