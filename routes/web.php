<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
  Route::get('about', 'HomeController@about')->name('about');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
	Route::get('/', 'AdminController@login')->name('admin.login');
	Route::group(['middleware' => ['isAdmin']], function(){
    	Route::get('/dashboard', ['as' => 'admin.dashboard.index', 'uses' => 'AdminController@index' ]);
      Route::get('users', 'UserController@index')->name('admin.users.index');
Route::get('users/create', 'UserController@create')->name('admin.users.create');
Route::get('users/edit/{id}', 'UserController@edit')->name('admin.users.edit');
Route::get('users/destroy/{id}', 'UserController@destroy')->name('admin.users.destroy');
Route::get('users/change-status/{id}', 'UserController@changeStatus')->name('admin.users.change-status');
Route::post('users/store', 'UserController@store')->name('admin.users.store');
Route::post('users/update', 'UserController@update')->name('admin.users.update');
Route::get('validation/user', 'UserController@unique_user')->name('validate.unique_user');


Route::get('roles', 'RoleController@index')->name('admin.roles.index');
Route::get('roles/create', 'RoleController@create')->name('admin.roles.create');
Route::get('roles/edit/{id}', 'RoleController@edit')->name('admin.roles.edit');
Route::get('roles/destroy/{id}', 'RoleController@destroy')->name('admin.roles.destroy');
Route::post('roles/store', 'RoleController@store')->name('admin.roles.store');
Route::post('roles/update', 'RoleController@update')->name('admin.roles.update');

Route::get('blog', 'BlogController@index')->name('admin.blog.index');
  Route::get('blog/create', 'BlogController@create')->name('admin.blog.create');
  Route::get('blog/edit/{id}', 'BlogController@edit')->name('admin.blog.edit');
  Route::get('blog/destroy/{id}', 'BlogController@destroy')->name('admin.blog.destroy');
  Route::get('blog/change-status/{id}', 'BlogController@changeStatus')->name('admin.blog.chang e-status');
  Route::post('blog/store', 'BlogController@store')->name('admin.blog.store');
  Route::post('blog/update', 'BlogController@update')->name('admin.blog.update');

  Route::get('service', 'ServiceController@index')->name('admin.service.index');
    Route::get('service/create', 'ServiceController@create')->name('admin.service.create');
    Route::get('service/edit/{id}', 'ServiceController@edit')->name('admin.service.edit');
    Route::get('service/destroy/{id}', 'ServiceController@destroy')->name('admin.service.destroy');
    Route::get('service/change-status/{id}', 'ServiceController@changeStatus')->name('admin.service.chang e-status');
    Route::post('service/store', 'ServiceController@store')->name('admin.service.store');
    Route::post('service/update', 'ServiceController@update')->name('admin.service.update');

  Route::get('mainpage', 'MainpageController@index')->name('admin.mainpage.index');
          Route::get('mainpage/create', 'MainpageController@create')->name('admin.mainpage.create');
          Route::get('mainpage/edit/{id}', 'MainpageController@edit')->name('admin.mainpage.edit');
          Route::get('mainpage/destroy/{id}', 'MainpageController@destroy')->name('admin.mainpage.destroy');
          Route::get('mainpage/change-status/{id}', 'MainpageController@changeStatus')->name('admin.mainpage.chang e-status');
          Route::post('mainpage/store', 'MainpageController@store')->name('admin.mainpage.store');
          Route::post('mainpage/update', 'MainpageController@update')->name('admin.mainpage.update');


  Route::get('job-openings', 'JobController@index')->name('admin.job-openings.index');
            Route::get('job-openings/create', 'JobController@create')->name('admin.job-openings.create');
            Route::get('job-openings/edit/{id}', 'JobController@edit')->name('admin.job-openings.edit');
            Route::get('job-openings/destroy/{id}', 'JobController@destroy')->name('admin.job-openings.destroy');
            Route::get('job-openings/change-status/{id}', 'JobController@changeStatus')->name('admin.job-openings.chang e-status');
            Route::post('job-openings/store', 'JobController@store')->name('admin.job-openings.store');
            Route::post('job-openings/update', 'JobController@update')->name('admin.job-openings.update');

});
	});
